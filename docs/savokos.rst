Sąvokos
#######

.. glossary::

    atvirų duomenų direktyva
        2019 m. birželio 20 d. Europos Parlamento ir Tarybos direktyva (ES)
        `2019/1024`_ dėl atvirųjų duomenų ir viešojo sektoriaus informacijos
        pakartotinio naudojimo.

        .. _2019/1024: https://eur-lex.europa.eu/legal-content/LT/TXT/?uri=CELEX:32019L1024

    duomenų valdymo aktas
        2020 m. lapkričio 25 d. Europos Parlamento ir Tarybos reglamento (ES)
        pasiūlymas `2020/0340`_ dėl Europos duomenų valdymo (Duomenų valdymo
        aktas).

        .. _2020/0340: https://eur-lex.europa.eu/legal-content/LT/TXT/?uri=CELEX:52020PC0767

    aplinkos kintamasis
        Angliškai tai vadinama *environment variables*, tai yra operacinės
        sistemos aplinkos kintamieji.

        Plačiau apie tai skaitykite `Vikipedijoje
        <https://en.wikipedia.org/wiki/Environment_variable>`__.

    ADP
        Atvirų duomenų portalas, sudarytas iš atvirų duomenų katalogo ir duomenų
        saugyklos.

    ADK
        Lietuvos atvirų duomenų katalogas, prieinamas adresu `data.gov.lt`_.

        .. _data.gov.lt: https://data.gov.lt/

    ADS
        Atvirų duomenų saugykla.

    DSA
        :ref:`Duomenų struktūros aprašas <duomenų-struktūros-aprašas>` yra
        lentelė, kurioje išsamiai aprašyta tam tikro duomenų šaltinio duomenų
        struktūra. DSA lentelę sudaro penkios dimensijos (duomenų rinkinys,
        resursas, bazė, modelis, savybė) ir dešimt metaduomenų stulpelių.

    ADSA
        :term:`DSA` lentelė, kurioje aprašomi jau atverti ir viešai prieinami
        duomenys.

    ŠDSA
        :term:`DSA` lentelė, kurioje aprašoma neatvertų, :term:`pirminio
        duomenų šaltinio <pirminis duomenų šaltinis>` duomenų struktūra.

    didelės vertės duomenys
    aukštos vertės duomenys
        Duomenys apibrėžti :term:`atvirų duomenų direktyvos <atvirų duomenų
        direktyva>` 5 skyriuje.

        `Aukštos vertės duomenų sritys`__ yra šios:

        .. __: https://eur-lex.europa.eu/legal-content/LT/TXT/?qid=1561563110433&uri=CELEX:32019L1024#d1e32-79-1

        - Geoerdviniai duomenys

        - Aplinka ir žemės stebėjimai

        - Meteorologiniai duomenys

        - Statistika (demografiniai ir ekonominiai rodikliai)

        - Įmonės ir įmonių savininkai

        - Judumas

    BDAR
        2016 m. balandžio 27 d. Europos Parlamento ir Tarybos reglamentas (ES)
        `2016/679`_ dėl fizinių asmenų apsaugos tvarkant asmens duomenis ir dėl
        laisvo tokių duomenų judėjimo ir kuriuo panaikinama Direktyva
        `95/46/EB`_ (Bendrasis duomenų apsaugos reglamentas).

        .. _2016/679: https://eur-lex.europa.eu/legal-content/LT/TXT/?uri=CELEX:32016R0679
        .. _95/46/EB: https://eur-lex.europa.eu/legal-content/LT/TXT/?uri=CELEX:31995L0046

    duomenų serializavimo formatas
        Duomenys gali būti serializuojami įvairiais formatais, pavyzdžiui YAML
        formatu:

        .. code-block:: yaml

           type: project
           title: Manifestas

        JSON formatu:

        .. code-block:: json

           {"type": "project", "title": "Manifestas"}

        Turtle formatu:

        .. code-block:: ttl

           @prefix foaf: <http://xmlns.com/foaf/0.1/> .
           @prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
           <http://atviriduomenys.lt> a foaf:Project;
               rdfs:label "Manifestas" .

        MessagePack dvejetainiu formatu, kurio turinys pateiktas naudojant BASE64
        koduotę::

           gqR0eXBlp3Byb2plY3SkbmFtZapNYW5pZmVzdGFz

        Visuose šiuose pavyzdžiuose yra pateikti tie patys duomenys, tačiau
        naudojami skirtingi duomenų serializavimo formatai, koduotės ir skirtingi
        žodynai.

    kanoniniai duomenys
        Kanoniniai duomenys yra tarsi duomenų etalonas, kuris nusako kokios
        duomenų reikšmės yra teisingos. Pavyzdžiui įmonės pavadinimas gali būti
        užrašomas įvairiausiomis formomis, pavyzdžiui:

        ============ =================================
        Įmonės kodas Įmonės pavadinimas
        ============ =================================
        \-           UAB "Duomesta"
        \-           UAB „Duomesta“
        \-           Duomesta
        \-           DUOMESTA
        \-           Uždaroji akcinė bendrovė Duomesta
        \-           Duomesta, UAB
        \-           DSTA UAB
        ============ =================================

        Jei duomenų rinkinyje nėra pateiktas įmonės registracijos kodas, tada
        unikaliai identifikuoti įmonę yra gan sudėtinga.

        Tačiau turint autoritetingus kanoninius duomenis:

        ============ =================================
        Įmonės kodas Įmones pavadinimas
        ============ =================================
        111111111    UAB "Duomesta"
        ============ =================================

        Užduotis unikaliai identifikuoti įmonę pasidaro paprastesnė. Todėl
        kanoniniai duomenys yra labai svarbūs.

    kodinis pavadinimas
        Pavadinimas, kuriam keliami tam tikri apribojimai.

    manifestas
        Atvirų duomenų manifestas yra :term:`DSA` lentelių rinkinys, kuriuose
        aprašyti duomenų šaltiniai ir juose esančių duomenų struktūra.

        Žodis manifestas yra kilęs iš programavimo srityje naudojamo termino
        `Manifesto failas`__, kuriame pateikiami metaduomenys apie programinio
        paketo sandarą.

        .. __: https://en.wikipedia.org/wiki/Manifest_file

        Duomenų kontekste, žodis manifestas turėtu būti suprantamas, kaip
        metaduomenų lentelė apie įvairiuose duomenų šaltiniuose publikuojamus
        duomenis.

    metaduomenys
        Duomenys apie duomenis yra vadinami metaduomenimis. Pavyzdžiui duomenų
        struktūros aprašas konkrečiam CSV duomenų failui gali būti vadinamas CSV
        failo metaduomenimis.

    normalizavimas
        Duomenų normalizavimas yra duomenų struktūros transformavimo procesas
        taikant taip vadinamas normalines formas, tam kad sumažinti duomenų
        pasikartojimą.

        Plačiau apie tai skaitykite `Vikipedijoje
        <https://en.wikipedia.org/wiki/Database_normalization>`__.

    prieigos taškas
        Prieigos taškas yra :term:`REST API` terminas, nurodantis URL kelio dalį iki tam
        tikro resurso.

        Plačiau skaitykite `Vikipedijoje
        <https://en.wikipedia.org/wiki/Web_API#Endpoints>`__.

    REST API
        Representational State Transfer (REST) yra taisyklių ir rekomendacijų
        rinkinys sirtas :term:`web servisams <web servisas>` kurti.

        Plačiau skaitykite `Vikipedijoje
        <https://en.wikipedia.org/wiki/Representational_state_transfer>`__.

    web servisas
        Web servisas yra interneto paslauga skirta automatizuotiems robotams.
        Interneto svetainės dažniausiai yra skirtos žmonėms, tačiau web servisai
        yra skirti mašioms, kurios gali komunikuoti viena su kita.

        Plačiau skaitykite `Vikipedijoje
        <https://en.wikipedia.org/wiki/Web_service>`__.

    YAML
        YAML yra :term:`duomenų serializavimo formatas`, kuris skirtas ne tik
        mašininiam skaitymui, bet su šio formato turiniu tiesiogiai gali dirbti
        ir žmogus. YAML formato pavyzdys:

        .. code-block:: yaml

           container:
             name: value

        YAML yra sukurtas JSON formatu pagrindu, siekant palengvinti darbą su
        JSON serializuotais duomenimis žmonėms. Analogiškas pavyzdys JSON formatu
        atrodo taip:

        .. code-block:: json

           {"container": {"name": "value"}}

    viešasis žodynas
        Viešieji žodynai, dar vadinami ontologijomis, šie žodynai dažnai yra
        gerai dokumentuoti ir skelbiami viešai, jie yra skirti globaliam
        susietųjų duomenų tinkui kurti (angl. *linked data*).

    sisteminis pavadinimas
        Sisteminis pavadinimas yra naudojamas objektų identifikavimui ir yra
        naudojamas URL nuorodose ir visur kitur, kure reikia nurodyti ryšį su
        objektų, naudojamas to objekto sisteminis pavadinimas.

        Sisteminis pavadinimas sudaromas tik iš lotyniškų raidžių ir `-_/`
        simbolių.

    pirminis duomenų šaltinis
        Įstaigos ar kitos organizacijos pagrindinis duomenų šaltinis.

    duomenų rinkinys
        Duomenų grupė charakterizuojanti :term:`modelį <modelis>` arba
        susijusius modelius jų :term:`savybes <savybė>` ir tarpusavio ryšius.
        Sąsaja tarp modelių apibrėžiama ne reliacinių ryšių prasme, o loginės
        arba semantinės sąsajos prasme.

        Duomenys neskaidomi į skirtingus duomenų rinkinius, pagal vietos, laiko
        ar kitus kriterijus.

        Plačiau apie duomenų rinkinius skaitykite skyriuje
        :ref:`duomenų-rinkinys`.

    DCAT
        Duomenų katalogo žodynas (angl. `Data Catalog Vocabulary`_).

        .. _Data Catalog Vocabulary: https://www.w3.org/TR/vocab-dcat-2/

    bazė
        Bazė arba loginė klasė yra modelių grupė turinčių bendras savybes ir
        vienodą semantinę prasmę.

    dimensija
        Dimensija yra metaduomenų, aprašomų DSA lentelėje, grupė. DSA lentelėje
        metaduomenys skirstomi į tokias dimensijas:

        - duomenų rinkinys
        - resursas
        - bazė
        - modelis
        - savybė

        Kiekviena dimensija turi skirtingą metaduomenų detalumo lygį.

        Plačiau apie dimensijas: :ref:`dimensijos`.

    modelis
        Tai informacija kuria apibrėžiama tam tikros kategorijos informacinių
        objektų duomenų struktūra. :term:`DSA` lentelėje atitinka :data:`model`.
        Atitinka `rdfs:Class`_ arba duomenų lentelę.

        .. _rdfs:Class: https://www.w3.org/TR/rdf-schema/#ch_class

    savybė
        Duomenų :term:`modeliui <modelis>` priklausančių informacinių
        :term:`objektų <objektas>` savybė, pavyzdžiui miesto pavadinimas, šalis
        kuriai priklauso miestas. :term:`DSA` lentelėje atitinka
        :data:`property`. Atitinka `rdfs:Property`_ arba lentelės stulpelį.

        .. _rdfs:Property: https://www.w3.org/TR/rdf-schema/#ch_property

    objektas
        Vienas duomenų įrašas sudarytas iš savybių ir savybėms priskirtų
        reikšmių. Informacinis objektas turi turėti unikalų identifikatorių.
        Atitinka `rdfs:Resource`_ arba lentelės vieną eilutę.

        .. _rdfs:Resource: https://www.w3.org/TR/rdf-schema/#ch_resource

    žodynas
        Duomenų kontekste, žodynas yra susitarimas, kokiais pavadinimais
        vadinami objektai ir jų savybės. Dažniausiai kiekvienas duomenų rinkinys
        turi savo vidinį naudojamą žodyną, visas Lietuvos atvirų duomenų modelis
        turi savo vidinį žodyną, kuris suvienodina skirtingus duomenų rinkinių
        naudojamus žodynus. Yra :term:`viešieji žodynai <viešasis žodynas>`, dar
        vadinami ontologijomis, kurie yra skelbiami viešai ir skirti globaliam
        susietųjų duomenų tinklui kurti.

        Duomenų kontekste, žodynas yra tiesiog :term:`modelių <modelis>` ir
        :term:`savybių <savybė>` pavadinimų rinkinys. Skirtingi duomenų
        šaltiniai dažniausiai naudoja skirtingus žodynus, t.y. naudoja
        skirtingus :term:`modelių <modelis>` ir :term:`savybių <savybė>`
        pavadinimus.

        :term:`Duomenų struktūros aprašas <DSA>` leidžia skirtinguose duomenų
        šaltiniuose naudojamus pavadinimus suvienodinti, taip, kad visi
        šaltiniai naudotų vieningą žodyną.

        Vieningo žodyno sudarymas yra gan sudėtinga užduotis, todėl, :term:`DSA`
        leidžia prie vieningo žodyno pereiti palaipsniui:

        - pirmiausia sudaromas vieno duomenų rinkinio žodynas,

        - kuris palaipsniui transformuojamas į Lietuvos vieningą žodyną,

        - o Lietuvos vieningas žodynas palaipsniui transformuojamas į globalų
          žodyną, nurodant sąsajas su išoriniais žodynais ir standartais.

        Žodynai sudaromi pasitelkiant :ref:`vardų erdves <vardų-erdvės>`.
